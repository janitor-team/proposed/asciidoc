"""Module containing metadata about asciidoc."""

VERSION = (10, 1, 2)

__version__ = '.'.join(map(str, VERSION))
